import {lazy} from 'react';
import {DASHBOARD_PATH} from './Slugs';

const Dashboard = lazy(() => import('../components/pages/dashboard/Dashboard'));

const Routes = [
    {
        path: DASHBOARD_PATH,
        exact: true,
        isPrivate: false,
        component: Dashboard
    }
]

export default Routes;