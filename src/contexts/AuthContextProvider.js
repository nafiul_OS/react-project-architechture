import React, {createContext, useState} from 'react';

export const AuthContext = createContext();

const AuthContextProvider = ({children}) => {

    const [isLogin, setIsLogin] = useState(false);

    const login = (credential) => {
        setIsLogin(true);
    }

    const logout = () => {

    }

    return (
        <AuthContext.Provider
            value={{
                isLogin,
                login,
                logout
            }}
        >
            {children}
        </AuthContext.Provider>
    );
}

export default AuthContextProvider;