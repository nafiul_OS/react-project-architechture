import React from 'react';
import {DASHBOARD_PATH, LOGIN_PATH, PAGE_403_PATH, PAGE_404_PATH, PAGE_500_PATH} from '../routes/Slugs';
import {LoginOutlined, PieChartOutlined, StarOutlined} from '@ant-design/icons';

const Navs = [
    {
        key: 'dashboard',
        title: 'Dashboard',
        path: DASHBOARD_PATH,
        icon: <PieChartOutlined/>,
        subMenu: null
    },
    {
        key: 'pages',
        title: 'Pages',
        icon: <StarOutlined/>,
        subMenu: [
            {
                key: 'login',
                title: 'Login',
                path: LOGIN_PATH,
                icon: <LoginOutlined/>,
                subMenu: null
            },
            {
                key: 'page403',
                title: 'Page403',
                path: PAGE_403_PATH,
            },
            {
                key: 'page404',
                title: 'Page404',
                path: PAGE_404_PATH,
            },
            {
                key: 'page500',
                title: 'Page500',
                path: PAGE_500_PATH,
            }
        ]
    }
]

export default Navs;