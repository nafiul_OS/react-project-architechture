import React, {useContext} from 'react';
import {Avatar, Dropdown, Layout, Menu} from 'antd';
import {Link} from 'react-router-dom';
import {UserOutlined} from '@ant-design/icons';

/* SCSS */
import './nav_header.scss'
import {AuthContext} from '../../../contexts/AuthContextProvider';

const {Header} = Layout;

const NavHeader = () => {

    const authContext = useContext(AuthContext);

    const logout = () => {
        authContext.logout();
    }

    const menu = (
        <Menu style={{minWidth: "120px", backgroundColor: "#ffffff"}}>
            <Menu.Item key="0">
                <Link to="">Profile</Link>
            </Menu.Item>
            <Menu.Divider/>
            <Menu.Item key="1" onClick={logout}>
                Logout
            </Menu.Item>
        </Menu>
    );

    return (
        <Header className="nav_header">
            <Dropdown className="drop_down" overlay={menu} trigger={['click']}>
                <div>
                    <span>admin@ecourier.org</span> &nbsp;
                    <Avatar icon={<UserOutlined/>}/>
                </div>
            </Dropdown>
        </Header>
    );
}

export default NavHeader;