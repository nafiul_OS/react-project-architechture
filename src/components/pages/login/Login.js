import React, {useContext} from 'react';
import {Button, Checkbox, Form, Input} from 'antd';
import {Redirect} from 'react-router-dom';

/* SCSS */
import './login.scss';
import {ROOT_PATH} from '../../../routes/Slugs';
import {AuthContext} from "../../../contexts/AuthContextProvider";


const Login = (props) => {

    const authContext = useContext(AuthContext);

    const onFinish = values => {
        console.log('Success:', values);
        authContext.login(values)
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const layout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    };
    const tailLayout = {
        wrapperCol: {offset: 8, span: 16},
    };

    if (authContext.isLogin) return <Redirect to={ROOT_PATH}/>

    return (
        <div className="login_form_wrapper">
            <Form
                {...layout}
                name="basic"
                initialValues={{remember: true}}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                className="login_form"
            >
                <h4 className="login_title">Login</h4>

                <Form.Item
                    label="Username"
                    name="username"
                    rules={[{required: true, message: 'Please input your username!'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{required: true, message: 'Please input your password!'}]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                </Form.Item>

                <Form.Item {...tailLayout}>
                    <Button type="primary" htmlType="submit">
                        Submit
                    </Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default Login;
